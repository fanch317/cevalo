Cevalo - A platform file delivery
===

What is cevalo ?
---

Cevalo is a Web platform for exchange file with your customers and partners. You can create exchance spaces (projects), and assign roles for your team, external users or via encoded url for accountless users.

Cevalo is a repository files for working and ended projects. You can use-it for validate files with approval system, and final delivery for public downloads. Supported files is image, vidéo, zip, ipa/apk, html, panorama.

Cevalo is writen on Symfony/PHP/MySQL and can be deployed by docker. The inital page project is https://gitlab.qth.fr/cevalo/issues

### Features
  - [ ] User/role management
  - [ ] SSO availables: Internal, OAuth, ldap and OpenID
  - [ ] Public acces for accountless
  - [ ] Theme customization
  - [ ] File types managed: images, video, archive, ipa, apk, html+css, exe
  - [ ] Approval system
  - [ ] Versionning files
  - [ ] Email notification
  - [ ] Push notification
  - [ ] Dashboard for roles
  - [ ] Project steps (planned, working, closed, archived, deleted)
  - [ ] Export/Import project
  - [ ] Statistics
  - [ ] Actions logging


Requirements
---

For a docker deployment :
  - Docker and docker-compose

For a traditional deployment :
  - Nginx/Apache
  - PHP v7.1+
  - MySQL 5.7.8+ or MariaDB 10.2.7+
  - PHP-OPcache and PHP-APCu recommended
  - Composer

Installation
---

Run docker-compose up



Fallback
---

Please go to https://gitlab.qth.fr/cevalo/cevalo/issues and search for duplicate issue before sumit your own.